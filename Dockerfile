FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y apt-transport-https
RUN apt-get install -y wget
RUN wget -q https://sensu.global.ssl.fastly.net/apt/pubkey.gpg -O- | apt-key add -
RUN echo "deb     https://sensu.global.ssl.fastly.net/apt sensu main" | tee /etc/apt/sources.list.d/sensu.list
RUN apt-get update
RUN apt-get install -y sensu supervisor cron git
RUN apt-get install -y bc sysstat sudo ruby-dev g++ make

COPY configs/config.json /etc/sensu/config.json
COPY configs/client.json /etc/sensu/conf.d/client.json
COPY configs/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY gitclient.sh /config/gitclient.sh
RUN chmod +x /config/gitclient.sh
#RUN ln -s /opt/sensu/embedded/bin/ruby /usr/bin/ruby
#RUN ln -s /opt/sensu/embedded/bin/gem /usr/bin/gem
RUN gem install sensu-plugins-kubernetes
RUN gem install sensu-plugins-prometheus-checks
#RUN gem install activesupport

# Entrypoint setting
ADD ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]